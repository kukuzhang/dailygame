﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using Kuku;
using UnityEngine.UI;

public class levelCtr : MonoBehaviour
{
    public GameObject UI_GAMEOVER;
    public GameObject UI_GAMEWIN;
    public GameObject balls;
    public GameObject lvsObj;
    public Text UI_tickCount;
    public Text UI_level;
    public Text UI_fail;

    private int fail_count = 0;
    
    public int tickCount = 3;

    public int lv = 1;
    // Start is called before the first frame update
    void Start()
    {

        lvsObj = GameObject.Find("lvs");
        getLv(lv).SetActive(true);
        balls = lvsObj.transform.Find("lv"+lv+"/balls").gameObject;
        
        EventCenter.Self.AddEventListener("Actor.Collision" ,onActorColl);
        EventCenter.Self.AddEventListener("Actor.click" ,onActorClick);
        UI_level.text = lv.ToString();
        UI_tickCount.text = "99";
        UI_tickCount.transform.parent.gameObject.SetActive(false);

    }

    GameObject getLv(int lvID)
    {
        return lvsObj.transform.Find("lv" + lvID)?.gameObject;
    }
    
    void onActorClick()
    {
        bool bAllMove = true;
        foreach (Transform ball in balls.transform)
        {
            if (ball.gameObject.GetComponent<ball>().status != BALL_STATUS.MOVE)
            {
                bAllMove = false;
            }   
        }

        if (bAllMove)
        {
           // UI_GAMEWIN.SetActive(true);
           StartCoroutine(onTimer());
        }
        
    }


    IEnumerator onTimer()
    {

        tickCount = 3;
        UI_tickCount.transform.parent.gameObject.SetActive(true);
        do
        {
            UI_tickCount.text = tickCount.ToString();
            yield return new WaitForSeconds(1f);
            tickCount--;

        } while (tickCount > 0);

        onGameWin();
    }

    void onGameWin()
    {
        gamePause();
        UI_tickCount.transform.parent.gameObject.SetActive(false);
        UI_GAMEWIN.SetActive(true);
      
    }

    public void gamePause()
    {
        StopAllCoroutines();
        foreach (Transform ball in balls.transform)
        {
            ball.gameObject.GetComponent<ball>().onStop();
        }
    }
    public void gameRestart()
    {
        UI_tickCount.text = "99";
        UI_GAMEOVER.SetActive(false);
      
        foreach (Transform ball in balls.transform)
        {
            ball.gameObject.GetComponent<ball>().onRestart();
        }
        
    }
    void onActorColl()
    {
        gamePause();
        UI_GAMEOVER.SetActive(true);
        UI_tickCount.transform.parent.gameObject.SetActive(false);
        fail_count++;
        UI_fail.text = fail_count.ToString();
    }

    public  void onNextLv()
    {
        clearVectorLine();
        if (getLv(lv+1) == null)
        {
            Debug.Log("已经是最后一关");
            return;
        }
        
        UI_GAMEWIN.SetActive(false);
        getLv(lv).SetActive(false);
        lv++;
        getLv(lv).SetActive(true);
        UI_level.text = lv.ToString();
        balls = getLv(lv).transform.Find("balls").gameObject;
        UI_tickCount.text = "99";
    }

    void clearVectorLine()
    {
        var vct = GameObject.Find("VectorCanvas");

        foreach (Transform line in vct.transform)
        {
            Destroy(line.gameObject);
        }
        
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
