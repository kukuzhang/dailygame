﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using Vectrosity;
using Kuku;

public enum BALL_STATUS
{
    STOP,
    MOVE,
    
}
public class ball : MonoBehaviour
{
    // Start is called before the first frame update
    public BALL_STATUS status;
    
    private DOTweenPath dtpath;
    void Start()
    {
        dtpath = GetComponent<DOTweenPath>();
        List<Vector3> poinList = new  List<Vector3>();
        
        poinList.Add(transform.position);
        foreach (var p in dtpath.wps)
        {
         poinList.Add(p);   
        }
        
        poinList.Add(transform.position);
        
      var vl = VectorLine.SetLine(Color.white,   poinList.ToArray());
     // vl.MakeCurve(poinList.ToArray());
     status = BALL_STATUS.STOP;
       
    }


    void OnMouseDown()
    {
        if (status == BALL_STATUS.MOVE)
            return;
        
        Debug.Log("鼠标点击");
       dtpath.DORestart();
       status = BALL_STATUS.MOVE;
       EventCenter.Self.EventTrigger("Actor.click");
    }

    
    private void OnCollisionEnter2D(Collision2D coll)
    {
        Debug.Log("碰撞发生(角色脚本)");
        EventCenter.Self.EventTrigger("Actor.Collision");
    }


    public void onStop()
    {
        dtpath.DOPause();
        status = BALL_STATUS.STOP;
    }

    public void onRestart()
    {
        
        dtpath.DORestart();
        dtpath.DOPause();
        status = BALL_STATUS.STOP;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
